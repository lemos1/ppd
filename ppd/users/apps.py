from django.apps import AppConfig


class UsersAppConfig(AppConfig):

    name = "ppd.users"
    verbose_name = "Users"

    def ready(self):
        try:
            import ppd.users.signals  # noqa F401
        except ImportError:
            pass
