from django import forms
from django.forms import ModelForm
from .models import Cadastro


class CadastroForm(forms.ModelForm):
    class Meta:
        model = Cadastro
        fields = [
                  'telefone', 'nome', 'rh', 'data_nascimento', 'data_diagnostico',
                  'diabetes', 'diagnostico', 'macroangiopatia', 'microangiopatia',
                  'nefropatia', 'neuropatia', 'medicacao',  'comorbidades', 'eventos',
                  'obs', 'telefone', 'ano'
                ]
        widgets = {
            'diabetes': forms.RadioSelect(),
            'diagnostico': forms.RadioSelect(),
            'nefropatia': forms.RadioSelect(),
            'neuropatia': forms.RadioSelect(),
            'eventos': forms.RadioSelect(),
            'obs': forms.Textarea(attrs={'rows': 3, 'cols': 15}),
            }
