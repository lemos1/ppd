from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from .models import Cadastro
from .forms import CadastroForm


@login_required
def lista_cadastro(request):
    cadastro = Cadastro.objects.all()
    data = {}
    data['object_list'] = cadastro
    return render(request, 'cadastros/lista_cadastro.html', data)


@login_required
def detalhe_cadastro(request, pk):
    cadastro = get_object_or_404(Cadastro, pk=pk)
    form = CadastroForm(request.POST or None, instance=cadastro)
    return render(request, 'cadastros/detalhe_cadastro.html', {'form': form, 'object': cadastro})


@login_required
def criar_cadastro(request):
    form = CadastroForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('lista_cadastro')
    return render(request, 'cadastros/form_cadastro.html', {'form': form})


@login_required
def editar_cadastro(request, pk):
    cadastro = get_object_or_404(Cadastro, pk=pk)
    form = CadastroForm(request.POST or None, instance=cadastro)
    if form.is_valid():
        form.save()
        return redirect('lista_cadastro')
    return render(request, 'cadastros/form_cadastro.html', {'form': form})


@login_required
def deletar_cadastro(request, pk):
    cadastro = get_object_or_404(Cadastro, pk=pk)
    if request.method == 'POST':
        cadastro.delete()
        return redirect('lista_cadastro')
    return render(request, 'deletar_confirmar.html', {'object': cadastro})
