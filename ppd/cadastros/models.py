from django.db import models
from system.models import BaseModel
from multiselectfield import MultiSelectField


class Cadastro(BaseModel):
    nome = models.CharField(
        max_length=80,
        blank=False,
        null=False,
        verbose_name='Nome',
    )
    rh = models.CharField(
        max_length=80,
        blank=False,
        null=False,
        verbose_name='RH',
    )
    data_nascimento = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=False,
        blank=False,
        verbose_name='Data de Nascimento',
    )
    data_diagnostico = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=False,
        blank=False,
        verbose_name='Data do Diagnóstico',
    )
    telefone = models.CharField(
        max_length=15,
        null=False,
        blank=False,
        verbose_name='Telefone'
    )

    DM1 = 'DM1'
    DM2 = 'DM2'
    MONOGENICO = 'Monogênico'
    POS = 'Pós-transplante'
    GESTACIONAL = 'Gestacional'
    OUTROS = 'Outros'

    DIABETES_TYPE_CHOICES = (
        (DM1, 'DM1'),
        (DM2, 'DM2'),
        (MONOGENICO, 'Monogênico'),
        (POS, 'Pós-transplante'),
        (GESTACIONAL, 'Gestacional'),
        (OUTROS, 'Outros'),
    )
    diabetes = models.CharField(
        max_length=80,
        blank=False,
        null=False,
        choices=DIABETES_TYPE_CHOICES,
        default=DM1,
        verbose_name='Tipo de Diabetes',
    )

    CAD = 'CAD'
    EHH = 'EHH'
    ROTINA = 'Rotina'

    DIAGNOSTICO_TYPE_CHOICES = (
        (CAD, 'CAD'),
        (EHH, 'EHH'),
        (ROTINA, 'Rotina'),
    )
    diagnostico = models.CharField(
        max_length=80,
        blank=False,
        null=False,
        choices=DIAGNOSTICO_TYPE_CHOICES,
        default=CAD,
        verbose_name='Sistomas no Diagnóstico',
    )

    AVE = 'AVE'
    IAM = 'IAM'
    DAC = 'DAC'
    DAP = 'DAP'
    ICC = 'ICC'

    MACROANGIOPATIA_TYPE_CHOICES = (
        (AVE, 'AVE'),
        (IAM, 'IAM'),
        (DAC, 'DAC'),
        (DAP, 'DAP'),
        (ICC, 'ICC'),
    )
    macroangiopatia = MultiSelectField(
        max_length=80,
        blank=True,
        null=True,
        choices=MACROANGIOPATIA_TYPE_CHOICES,
        verbose_name='Macroangiopatia',
    )

    NENHUM = 'Nenhum'
    RETINOPATIA = 'Retinopatia'
    NAO_PROLIFERATIVA = 'Não proliferativa'
    PROLIFERATIVA = 'Proliferativa'

    MICROANGIOPATIA_TYPE_CHOICES = (
        (NENHUM, 'Nenhum'),
        (RETINOPATIA, 'Retinopatia'),
        (NAO_PROLIFERATIVA, 'Não proliferativa'),
        (PROLIFERATIVA, 'Proliferativa'),
    )
    microangiopatia = MultiSelectField(
        max_length=80,
        blank=True,
        null=True,
        choices=MICROANGIOPATIA_TYPE_CHOICES,
        verbose_name='Microangiopatia',
    )

    NENHUM = 'Nenhum'
    NEFROPATIA = 'Nefropatia'
    DIALITICA = 'Dialítica'
    NAO_DIALITICA = 'Não dialítica'
    ALBUMINURIA = 'Albuminúria'

    NEFROPATIA_TYPE_CHOICES = (
        (NENHUM, 'Nenhum'),
        (NEFROPATIA, 'Nefropatia'),
        (DIALITICA, 'Dialítica'),
        (NAO_DIALITICA, 'Não dialítica'),
        (ALBUMINURIA, 'Albuminúria'),
    )
    nefropatia = models.CharField(
        max_length=80,
        blank=True,
        null=True,
        choices=NEFROPATIA_TYPE_CHOICES,
        verbose_name='Nefropatia',
    )

    NENHUM = 'Nenhum'
    NEUROPATIA = 'Neuropatia autonômica'
    HIPOTENSAO = 'Hipotensão postural'
    GASTROPARESIA = 'Gastroparesia'
    DISFUNCAO = 'Disfunção erétil'
    HIPOGLICEMIA = 'Hipoglicemia oligo/assintomática'
    PERIFERICA = 'Neuropatia Periférica'

    NEUROPATIA_TYPE_CHOICES = (
        (NENHUM, 'Nenhum'),
        (NEUROPATIA, 'Neuropatia autonômica'),
        (HIPOTENSAO, 'Hipotensão postural'),
        (GASTROPARESIA, 'Gastroparesia'),
        (DISFUNCAO, 'Disfunção erétil'),
        (HIPOGLICEMIA, 'Hipoglicemia oligo/assintomática'),
        (PERIFERICA, 'Neuropatia Periférica'),
    )
    neuropatia = models.CharField(
        max_length=80,
        blank=True,
        null=True,
        choices=NEUROPATIA_TYPE_CHOICES,
        verbose_name='Neuropatia automômica',
    )

    INSULINA = 'Insulina'
    NPH = 'NPH'
    REGULAR = 'Regular'
    LENTA = 'Análoga lenta'
    RAPIDA = 'Anãloga ultra-rápida'
    BCI = 'BCI'
    DROGAS = 'Drogas orais'
    GLP = 'Agonista de GLP-1'
    DPP = 'Inibidor de DPP-4'
    SGLT = 'Inibidor de SGLT2'
    PIOGLITAZONA = 'Pioglitazona'
    SULFONILUREIAS = 'Sulfoniluréias'
    METFORMINA = 'Metformina'

    MEDICACAO_TYPE_CHOICES = (
        (INSULINA, 'Insulina'),
        (NPH, 'NPH'),
        (REGULAR, 'Regular'),
        (LENTA, 'Análoga lenta'),
        (RAPIDA, 'Análoga ultra-rápida'),
        (BCI, 'BCI'),
        (DROGAS, 'Drogas orais'),
        (GLP, 'Agonista de GLP-1'),
        (DPP, 'Inibidor de DPP-4'),
        (SGLT, 'Inibidor de SGLT2'),
        (PIOGLITAZONA, 'Pioglitazona'),
        (SULFONILUREIAS, 'Sulfoniluréias'),
        (METFORMINA, 'Metformina'),
    )
    medicacao = MultiSelectField(
        max_length=80,
        blank=True,
        null=True,
        choices=MEDICACAO_TYPE_CHOICES,
        verbose_name='Medicação atual',
    )

    HAS = 'HAS'
    DLP = 'DLP'
    NEOPLASIA = 'Neoplasia'

    COMORBIDADES_TYPE_CHOICES = (
        (HAS, 'HAS'),
        (DLP, 'DLP'),
        (NEOPLASIA, 'Neoplasia'),
    )
    comorbidades = MultiSelectField(
        max_length=80,
        blank=True,
        null=True,
        choices=COMORBIDADES_TYPE_CHOICES,
        verbose_name='Comorbidades',
    )

    IAM = 'IAM'
    AVE = 'AVE'
    OUTRAS = 'Outras'

    EVENTOS_TYPE_CHOICES = (
        (IAM, 'IAM'),
        (AVE, 'AVE'),
        (OUTRAS, 'Outras'),
    )
    eventos = models.CharField(
        max_length=80,
        blank=True,
        null=True,
        choices=EVENTOS_TYPE_CHOICES,
        verbose_name='Eventos fatais',
    )

    obs = models.TextField(
        max_length=999,
        blank=True,
        null=True,
        verbose_name='Obs',
    )
    ano = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True,
        verbose_name='Ano',
    )

    def __str__(self):
        return self.nome

    class Meta:
        verbose_name = 'Nome'
