from django.urls import path
from .views import (
    lista_cadastro,
    detalhe_cadastro,
    criar_cadastro,
    editar_cadastro,
    deletar_cadastro
)


urlpatterns = [
    path("", lista_cadastro, name="lista_cadastro"),
    path("<int:pk>", detalhe_cadastro, name="detalhe_cadastro"),
    path("novo/", criar_cadastro, name="criar_cadastro"),
    path("<int:pk>/editar", editar_cadastro, name="editar_cadastro"),
    path("<int:pk>/deletar", deletar_cadastro, name="deletar_cadastro"),
]
