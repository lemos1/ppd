from django import forms
from allauth.account.forms import (
    LoginForm,
)


class CustomLoginForm(LoginForm):
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(CustomLoginForm, self).__init__(*args, **kwargs)
        self.fields['login'].widget.attrs.update({'class': 'form-control', 'type': 'text'})
        self.fields['password'].widget.attrs.update({'class': 'form-control', 'type': 'text'})
