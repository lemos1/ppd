from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from cadastros.models import Cadastro


@login_required
def home(request):
    cadastros = Cadastro.objects.all().count()
    return render(request, 'pages/home.html', {
        'cadastros': cadastros,
    })
